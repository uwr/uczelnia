import requests
import time
import numpy as np
import schedule
import re
import csv
url = "https://danepubliczne.imgw.pl/api/data/synop/format/csv"
r = requests.get(url, allow_redirects=True)
data = r.content
data = data.splitlines()
string2 = str(data).replace(" b'", '')
string2 = str(string2).replace("\\xc5\\x82","ł")
string2 = str(string2).replace("\\xc5\\x81","Ł")
string2 = str(string2).replace("\\xc5\\x84","ń")
string2 = str(string2).replace("\\xc5\\x83","Ń")
string2 = str(string2).replace("\\xc5\\x9b","ś")
string2 = str(string2).replace("\\xc5\\x9a","Ś")
string2 = str(string2).replace("\\xc4\\x87","ć")
string2 = str(string2).replace("\\xc4\\x86","ć")
string2 = str(string2).replace("\\xc3\\xb3","ó")
string2 = str(string2).replace("\\xc3\\x93","Ó")
string2 = str(string2).replace("\\xc5\\xbc","ż")
string2 = str(string2).replace("\\xc5\\xbb","Ż")
string2 = str(string2).replace("\\xc4\\x85","ą")
string2 = str(string2).replace("\\xc4\\x84","Ą")
string2 = str(string2).replace("\\xc4\\x99","ę")
string2 = str(string2).replace("\\xc4\\x98","Ę")
string2 = str(string2).replace("\\xc5\\xba","ź")
string2 = str(string2).replace("\\xc5\\xb9","Ź")
stringi = string2.split(',')
mac = np.array(stringi).reshape(-1, 10)
kolumny = mac[:,[0, 1]]
print("Lista stacji do wyboru:")
print(kolumny)
numer = mac[:,[0]]
def sprawdzeniedaty():
    try:
        global do_kiedy 
        do_kiedy = input ("Wpisz datę, do której będą gromadzić się dane w formacie rok-miesiąc-dzień godzina:minuta:sekunda: ")
        end = time.strptime(do_kiedy, "%Y-%m-%d %H:%M:%S")
        roznica = time.mktime(end) - time.time()
        if roznica < 0:
            do_kiedy = input ("Podano datę z przeszłości, kliknij enter by kontynuować lub wpisz ABORT by anulować: ")
            if do_kiedy == "ABORT":
                exit()
            else:
                sprawdzeniedaty() 
    except:
            do_kiedy = input ("Format daty jest nieprawidłowy, kliknij enter by kontynuować lub wpisz ABORT by anulować: ")
            if do_kiedy == "ABORT":
                exit()
            else:
                sprawdzeniedaty()
while True:
    nr = input("Podaj numer stacji: ")
    if nr not in numer:
        print("Podano nieprawidłowy numer")
        nr = input("Podaj prawidłowy numer stacji, jeśli chcesz anulować proces wpisz ABORT: ")
        if nr == "ABORT":
            exit()
        if nr == "ABORT:":
            exit()
        elif nr in numer:
            break
        else:
            continue
    else:
        break
while True:
    try:
        do_kiedy = input ("Wpisz datę, do której będą gromadzić się dane w formacie rok-miesiąc-dzień godzina:minuta:sekunda: ")
        end = time.strptime(do_kiedy, "%Y-%m-%d %H:%M:%S")
        roznica = time.mktime(end) - time.time()
        if roznica < 0:
            do_kiedy = input ("Podano datę z przeszłości, kliknij enter by kontynuować lub wpisz ABORT by anulować: ")
            if do_kiedy == "ABORT":
                exit()
            else:
                sprawdzeniedaty()
    except:
            do_kiedy = input ("Format daty jest nieprawidłowy, kliknij enter by kontynuować lub wpisz ABORT by anulować: ")
            if do_kiedy == "ABORT":
                exit()
    else:
        print ("Dane będą pobierać się do:", do_kiedy)
        break
end=time.strptime(do_kiedy, "%Y-%m-%d %H:%M:%S") #powtórka z rozrywki bo poprzednia funkcja nie zapisywała tego globalnie ale może można zmienić po prostu zmienną end na global, nie sprawdzałem
def Convert(string): #definicja zamiany pobranego stringa na listę z której można wyciągnąć elementy
    li = list(string.split(","))
    return li
roznica = time.mktime(end) - time.time()
start = time.localtime()
start = time.mktime(start)
print("Rozpoczynam zbieranie danych")
temperatury = [] #tworzenie pustych list
daty = []
def robota3(): #definicja która będzie się powtarzać w pętli schedule, będzie tak robić co godzinę
    global data3
    link = "https://danepubliczne.imgw.pl/api/data/synop/id/"
    r2 = requests.get(link + nr, allow_redirects=True)
    data2 = r2.content
    data2 = str(data2)
    lista = Convert(data2)
    from operator import itemgetter
    data3 = itemgetter(2)(lista)   
    godzina = itemgetter(3)(lista)
    temperatura = itemgetter(4)(lista)
    godzina = re.sub('[aąbcćdeęfghijklłmnńoóprsśtuwyzźżAĄBCĆDEĘFGHIJKLŁMNŃOÓPRSŚTUWYZŹŻ_:""]', "", godzina)
    data3 = re.sub('[aąbcćdeęfghijklłmnńoóprsśtuwyzźżAĄBCĆDEĘFGHIJKLŁMNŃOÓPRSŚTUWYZŹŻ_:""]', "", data3)
    temperatura = re.sub('[aąbcćdeęfghijklłmnńoóprsśtuwyzźżAĄBCĆDEĘFGHIJKLŁMNŃOÓPRSŚTUWYZŹŻ_:""]', "", temperatura)
    data3 = data3 + " " + godzina + ":00:00"
    if data3 not in daty:
        daty.append(data3)
        temperatury.append(temperatura)
        print("Pobrano dane.")
    else:
        print("Brak nowych danych, czekam 5min.4")
        time.sleep(300)
def robota2(): #definicja która będzie się powtarzać w pętli schedule, będzie tak robić co godzinę
    global data3
    link = "https://danepubliczne.imgw.pl/api/data/synop/id/"
    r2 = requests.get(link + nr, allow_redirects=True)
    data2 = r2.content
    data2 = str(data2)
    lista = Convert(data2)
    from operator import itemgetter
    data3 = itemgetter(2)(lista)   
    godzina = itemgetter(3)(lista)
    temperatura = itemgetter(4)(lista)
    godzina = re.sub('[aąbcćdeęfghijklłmnńoóprsśtuwyzźżAĄBCĆDEĘFGHIJKLŁMNŃOÓPRSŚTUWYZŹŻ_:""]', "", godzina)
    data3 = re.sub('[aąbcćdeęfghijklłmnńoóprsśtuwyzźżAĄBCĆDEĘFGHIJKLŁMNŃOÓPRSŚTUWYZŹŻ_:""]', "", data3)
    temperatura = re.sub('[aąbcćdeęfghijklłmnńoóprsśtuwyzźżAĄBCĆDEĘFGHIJKLŁMNŃOÓPRSŚTUWYZŹŻ_:""]', "", temperatura)
    data3 = data3 + " " + godzina + ":00:00"
    for x in range (0,10):
        if data3 in daty:
            robota3()
        else:
            daty.append(data3)
            temperatury.append(temperatura)
            print("Pobrano dane.")
            break
def robota1(): #definicja która będzie się powtarzać w pętli schedule, będzie tak robić co godzinę
    global data3
    global start2
    global counter
    global temperatura
    start2 = time.time()
    link = "https://danepubliczne.imgw.pl/api/data/synop/id/"
    r2 = requests.get(link + nr, allow_redirects=True)
    data2 = r2.content
    data2 = str(data2)
    lista = Convert(data2)
    from operator import itemgetter
    data3 = itemgetter(2)(lista)   
    godzina = itemgetter(3)(lista)
    temperatura = itemgetter(4)(lista)
    godzina = re.sub('[aąbcćdeęfghijklłmnńoóprsśtuwyzźżAĄBCĆDEĘFGHIJKLŁMNŃOÓPRSŚTUWYZŹŻ_:""]', "", godzina)
    data3 = re.sub('[aąbcćdeęfghijklłmnńoóprsśtuwyzźżAĄBCĆDEĘFGHIJKLŁMNŃOÓPRSŚTUWYZŹŻ_:""]', "", data3)
    temperatura = re.sub('[aąbcćdeęfghijklłmnńoóprsśtuwyzźżAĄBCĆDEĘFGHIJKLŁMNŃOÓPRSŚTUWYZŹŻ_:""]', "", temperatura)
    temperatura = float(temperatura)
    data3 = data3 + " " + godzina + ":00:00"
    if data3 in daty:
        print("Brak nowych danych, czekam 5min.")
        time.sleep(300)
        robota2()
    else:
        print("Pobrano dane.")
        daty.append(data3)
        temperatury.append(temperatura)
        print(daty)
        print(temperatury)
schedule.every().hour.at(":01").do(robota1)
while True: 
    schedule.run_pending()
    if time.time() > (start + roznica):
        break
print(daty)
print(temperatury)
def Average(lst): 
    return sum(lst) / len(lst)
srednia = Average(temperatury)
przedzial1 = daty[0]
przedzial2 = daty[-1]
temperatury.append(srednia)
daty.append("Wartosc srednia temperatury to")
daty.append("Przedzial czasu to")
temperatury.append(przedzial1 + " - " + przedzial2)
file = open("wynik.txt", "w")
for index in range(len(daty)):
    file.write(str(daty[index]) + "\t" + str(temperatury[index]) + "\n")
file.close()