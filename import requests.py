import requests
import time
import numpy as np
url = "https://danepubliczne.imgw.pl/api/data/synop/format/csv"
r = requests.get(url, allow_redirects=True)
data = r.content
data = data.splitlines()
string2 = str(data).replace(" b'", '')
string2 = str(string2).replace("\\xc5\\x82","ł")
string2 = str(string2).replace("\\xc5\\x81","Ł")
string2 = str(string2).replace("\\xc5\\x84","ń")
string2 = str(string2).replace("\\xc5\\x83","Ń")
string2 = str(string2).replace("\\xc5\\x9b","ś")
string2 = str(string2).replace("\\xc5\\x9a","Ś")
string2 = str(string2).replace("\\xc4\\x87","ć")
string2 = str(string2).replace("\\xc4\\x86","ć")
string2 = str(string2).replace("\\xc3\\xb3","ó")
string2 = str(string2).replace("\\xc3\\x93","Ó")
string2 = str(string2).replace("\\xc5\\xbc","ż")
string2 = str(string2).replace("\\xc5\\xbb","Ż")
string2 = str(string2).replace("\\xc4\\x85","ą")
string2 = str(string2).replace("\\xc4\\x84","Ą")
string2 = str(string2).replace("\\xc4\\x99","ę")
string2 = str(string2).replace("\\xc4\\x98","Ę")
string2 = str(string2).replace("\\xc5\\xba","ź")
string2 = str(string2).replace("\\xc5\\xb9","Ź")
stringi = string2.split(',')
mac = np.array(stringi).reshape(-1, 10)
kolumny = mac[:,[0, 1]]
print("Lista stacji do wyboru:")
print(kolumny)
numer = mac[:,[0]]
while True:
    nr = input("Podaj numer stacji: ")
    if nr not in numer:
        print("Podano nieprawidłowy numer")
        nr = input("Podaj prawidłowy numer stacji, jeśli chcesz anulować proces wpisz ABORT: ")
        if nr == "ABORT":
            exit()
        if nr == "ABORT:":
            exit()
        elif nr in numer:
            break
        else:
            continue
    else:
        break
link = "https://danepubliczne.imgw.pl/api/data/synop/id/"
r2 = requests.get(link + nr, allow_redirects=True)
data2 = r2.content
print(data2)
data2 = str(data2)
string3 = str(data2).replace(" b'", '')
string3 = str(string3).replace("\\xc5\\x82","ł")
string3 = str(string3).replace("\\xc5\\x81","Ł")
string3 = str(string3).replace("\\xc5\\x84","ń")
string3 = str(string3).replace("\\xc5\\x83","Ń")
string3 = str(string3).replace("\\xc5\\x9b","ś")
string3 = str(string3).replace("\\xc5\\x9a","Ś")
string3 = str(string3).replace("\\xc4\\x87","ć")
string3 = str(string3).replace("\\xc4\\x86","ć")
string3 = str(string3).replace("\\xc3\\xb3","ó")
string3 = str(string3).replace("\\xc3\\x93","Ó")
string3 = str(string3).replace("\\xc5\\xbc","ż")
string3 = str(string3).replace("\\xc5\\xbb","Ż")
string3 = str(string3).replace("\\xc4\\x85","ą")
string3 = str(string3).replace("\\xc4\\x84","Ą")
string3 = str(string3).replace("\\xc4\\x99","ę")
string3 = str(string3).replace("\\xc4\\x98","Ę")
string3 = str(string3).replace("\\xc5\\xba","ź")
string3 = str(string3).replace("\\xc5\\xb9","Ź")
def Convert(string): 
    li = list(string.split(","))
    return li
lista = Convert(data2)
from operator import itemgetter   
import re 
godzina = itemgetter(3)(lista)
godzina = re.sub('[aąbcćdeęfghijklłmnńoóprsśtuwyzźżAĄBCĆDEĘFGHIJKLŁMNŃOÓPRSŚTUWYZŹŻ_:""]', "", godzina)
print(godzina)