import csv
import numpy as np
import glob
import os

dir_name = 'H:\Python\cw7'
os.chdir(dir_name)
lista = []
np.array(lista)
for filename in glob.glob(os.path.join(dir_name, "*.csv")):
    with open(os.path.join(os.getcwd(),filename),"r",encoding="utf8") as f:
        dane=csv.reader(f)
        #ignore headers
        next(dane)
        for row in dane:
            lista.append(row)
tablica = np.array(lista)
temperatura = tablica[:,4].astype(float) #wyciągamy kolumnę temperatury
min_temp = min(temperatura) #określamy minimalną wartość temperatury
mini = np.where(temperatura == min_temp) #znajdujemy wiersz w którym jest najmniejsza temperatura
max_temp = max(temperatura) #analogicznie
maxi = np.where(temperatura == max_temp) #analogicznie
print("Temperatura maksymalna została odnotowana w stacji ", end = '') #generowanie odpowiedzi
print(tablica[maxi,1], end = '')
print( " o godzinie ", end = '')
print(tablica[maxi,3], end = '')
print(" i wynosiła ona ", end = '')
print(max_temp)
print("Temperatura minimalna została odnotowana w stacji ", end = '')
print(tablica[mini,1], end = '')
print( " o godzinie ", end = '')
print(tablica[mini,3], end = '')
print(" i wynosiła ona ", end = '')
print(min_temp)
opady = tablica[:,8].astype(float) #analogicznie jak temperatura
stacje = np.where(opady > 0)
nazwy = np.array(tablica[stacje,1], order = "c") #stworzenie macierzy z nazwy
godziny = np.array(tablica[stacje,3], order = "c") #to samo tylko z godziny
num_columns = np.shape(nazwy)[1] #określenie liczby kolumn w macierzy, a jako że to macierz jednowymiarowa to zarazem liczby wyrazów
nazwy2 = nazwy.reshape((num_columns, 1)) #zmiana kształtu macierzy z jednego wiersza do jednej kolumny 
godziny2 = godziny.reshape((num_columns, 1)) #to samo
kombo = np.append(nazwy2, godziny2, axis = 1) #stworzenie jednej ładniejszej macierzy
print("Opady miały miejsce w tych stacjach w godzinach:")
print(kombo)