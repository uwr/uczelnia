import turtle
import math
turtle.setworldcoordinates(-400, 0, 400, 800)
szerokosc = 600
drzwi1 = 90
drzwi2 = 50
parter = 200
predkosc = 2
skret = 90
przekatna = (szerokosc*math.sqrt(2)/2)
zolw = turtle.Turtle()
zolw.speed(predkosc)
zolw.forward(szerokosc/2)
zolw.left(skret)
zolw.forward(parter)
zolw.left(skret)
zolw.forward(szerokosc)
zolw.right(skret/2*3)
zolw.forward(przekatna)
zolw.right(skret)
zolw.forward(przekatna)
zolw.right(skret/2*3)
zolw.forward(szerokosc)
zolw.left(skret)
zolw.forward(parter)
zolw.left(skret)
zolw.forward(szerokosc/2)
zolw.left(skret)
zolw.forward(drzwi1)
zolw.right(skret)
zolw.forward(drzwi2)
zolw.right(skret)
zolw.forward(drzwi1)
turtle.done()